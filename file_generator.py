import random
import string
from utils import timeit


def generate_string(max_string_length: int) -> str:
    """
    Generates single string from ascii letters + digits alphabet
    :param max_string_length: max length of the string
    :return:
    """
    return ''.join(random.choices(string.ascii_letters + string.digits, k=random.randint(0, max_string_length)))


@timeit
def big_data_file_generator(filename: str, string_count: int = 10, max_string_length: int = 10):
    """
    Generates random text file with given string count and max length of each string
    :param filename: output filename
    :param string_count: quantity of strings
    :param max_string_length: max length of each string (in symbols)
    :return:
    """
    with open(filename, 'a') as fout:
        for ind in range(string_count):
            rand_str = generate_string(max_string_length)
            fout.write(rand_str+"\n")