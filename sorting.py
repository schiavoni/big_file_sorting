from utils import timeit
from typing import List, Dict


def sort_lines_in_subset(subset_indexes: List[int], filename: str, shift_mapping: Dict[int, tuple]) -> List[int]:
    """
    Sort all lines of subset in memory
    :param subset_indexes: indexes of lines in input file in given subset
    :param filename: input filename
    :param shift_mapping: shifts of each line from start of the file
    :return: indexes of sorted lines
    """
    cur_lines = []
    with open(filename) as fin:
        for ind in subset_indexes:
            fin.seek(shift_mapping[ind][0])
            line = fin.read(shift_mapping[ind][1] - shift_mapping[ind][0])
            cur_lines.append((ind, line))

    new_indexes = [i[0] for i in sorted(cur_lines, key=lambda x: x[1])]

    return new_indexes


@timeit
def in_memory_sort(subdivision: List[int], input_filename: str, shift_mapping: Dict[int, tuple]) -> List[List[int]]:
    """
    Runs sorting subset of lines for all subsets
    :param subdivision: list of line indexes in current subset
    :param input_filename:
    :param shift_mapping: shifts of each line from start of the file
    :return: list of indexes in each sorted subset
    """
    for i, division in enumerate(subdivision):
        subdivision[i] = sort_lines_in_subset(division, input_filename, shift_mapping)
    return subdivision


def merge(first_ids: List[int], second_ids: List[int], filename: str, shift_mapping: Dict[int, tuple]) -> List[int]:
    """
    External merge sort for two subsets/files
    :param first_ids: line indexes in input file of first sorted subset
    :param second_ids: line indexes in input file of second sorted subset
    :param filename: input filename
    :param shift_mapping: shifts of each line from start of the file
    :return: list of sorted lines indexes of both subsets
    """
    merged_indexes = []
    with open(filename) as fin:
        i = 0
        j = 0
        while i < len(first_ids) and j < len(second_ids):
            fin.seek(shift_mapping[first_ids[i]][0])
            first_line = fin.read(shift_mapping[first_ids[i]][1] - shift_mapping[first_ids[i]][0])

            fin.seek(shift_mapping[second_ids[j]][0])
            second_line = fin.read(shift_mapping[second_ids[j]][1] - shift_mapping[second_ids[j]][0])

            if first_line < second_line:
                merged_indexes.append(first_ids[i])
                i += 1
                continue
            if first_line > second_line:
                merged_indexes.append(second_ids[j])
                j += 1
                continue

            merged_indexes.append(first_ids[i])
            merged_indexes.append(second_ids[j])
            i += 1
            j += 1

        if i < len(first_ids):
            merged_indexes.extend(first_ids[i:])
        if j < len(second_ids):
            merged_indexes.extend(second_ids[j:])

    return merged_indexes


@timeit
def external_merge_sort(subdivision: List[List[int]], input_filename: str, shift_mapping: Dict[int, tuple])-> List[int]:
    """
    Runs merge sort for all subsets
    :param subdivision: list of line indexes in all subsets
    :param input_filename:
    :param shift_mapping: shifts of each line from start of the file
    :return: line indexes in input file for sorted lines
    """
    while len(subdivision) > 1:
        new_subdivision = []
        for i in range(1, len(subdivision), 2):
            new_subdivision.append(merge(subdivision[i-1], subdivision[i], input_filename, shift_mapping))
        if len(subdivision) % 2 == 1:
            new_subdivision.append(subdivision[-1])
        subdivision = new_subdivision

    return subdivision[0]
