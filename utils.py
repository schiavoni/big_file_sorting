import functools
import time
from typing import List, Dict

def timeit(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        start = time.time()
        res_func = func(*args, **kwargs)
        end = time.time()
        print("Timing", func.__name__, end-start)
        return res_func
    return wrapper


@timeit
def save_sorted_file(result_data_filename: str, input_data_filename: str,
                     sorted_indexes: List[int], shift_mapping: Dict[int, tuple]):
    with open(input_data_filename) as fin:
        with open(result_data_filename, 'w') as fout:
            for i in sorted_indexes:
                fin.seek(shift_mapping[i][0])
                line = fin.read(shift_mapping[i][1] - shift_mapping[i][0])
                fout.write(line)