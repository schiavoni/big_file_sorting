from typing import List, Dict
from utils import timeit, save_sorted_file
from sorting import external_merge_sort, in_memory_sort
from file_generator import big_data_file_generator


@timeit
def split_file_on_subfiles(filename: str, buf_size: int) -> List[List[int]]:
    """
    Split full text file on subsamples less than buf_size
    :param filename: input text file filename
    :param buf_size: buf size (in symbpls)
    :return: list of list line indexes of each subsample
    """
    size = 0
    all_subsets = []
    subset = []
    with open(filename) as fin:
        for i, line in enumerate(fin):
            line = line.strip()
            if size + len(line) > buf_size:
                all_subsets.append(subset)
                subset = []
                size = 0
            subset.append(i)
            size += len(line)
        all_subsets.append(subset)

    return all_subsets


@timeit
def calculate_line_shifts(filename: str) -> Dict[int, tuple]:
    """
    Calculates shift from file start for each line in the file
    :param filename: input filename
    :return: mapping from line index in input file to start|end of the line (in bytes)
    """
    mapping = {}
    index = 0
    with open(filename) as fin:
        start = fin.tell()
        line = fin.readline()
        end = fin.tell()
        mapping[index] = (start, end)
        while line:
            start = fin.tell()
            line = fin.readline()
            end = fin.tell()
            index += 1
            mapping[index] = (start, end)

    return mapping


@timeit
def test_sort(input_filename, output_filename, buf_size=50000, string_count=1000000, max_string_length=10000):
    """
    Test function to check all is ok
    :param input_filename:
    :param output_filename:
    :param buf_size: buffer size
    :param string_count: quantity of strings in file to generate
    :param max_string_length: max length of each string
    :return:
    """
    # create test file
    big_data_file_generator(input_filename, string_count, max_string_length)

    # save shifts for each line from start of the file
    shift_mapping = calculate_line_shifts(input_filename)

    # split file to subfiles and sort it in memory
    subdivision: List[List] = split_file_on_subfiles(input_filename, buf_size)
    subdivision = in_memory_sort(subdivision, input_filename, shift_mapping)

    # external merge_sort:
    subdivision = external_merge_sort(subdivision, input_filename, shift_mapping)

    # write result to file
    save_sorted_file(output_filename, input_filename, subdivision, shift_mapping)


if __name__ == "__main__":
    test_sort("big_data_file.txt", "big_data_file_sorted.txt", buf_size=5000, string_count=10000,
              max_string_length=100)
